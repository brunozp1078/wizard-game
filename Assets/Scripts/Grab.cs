﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Grab : MonoBehaviour 
{
	public  Transform targetCam;
	private float distanceToObject = 5;

	[SerializeField]
	private GameObject goTargetted;

	[SerializeField]
	private GameObject goGrabbed;

	[SerializeField]
	private GameObject goHands;

	private Tweener tweenPull;




	public Transform player;
	public float throwforce;
	bool hasPlayer = false;
	bool beingCarried = false;
	private bool touched = false;

	// Use this for initialization
	void Start () 
	{
		goTargetted = null;
		goGrabbed = null;
		tweenPull = null;
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckTargettingObject();
		CheckMouseInput();
	}

	private void CheckTargettingObject()
	{
		RaycastHit hit;
		int interactLayer = 1 << LayerMask.NameToLayer ("Interact"); 

		if (Physics.Raycast (targetCam.position, targetCam.forward, out hit, distanceToObject, interactLayer)) 
		{
			goTargetted = hit.collider.gameObject;
		} 
		else
		{
			goTargetted = null;
		}
	}

	private void CheckMouseInput ()
	{
		if (Input.GetMouseButtonDown (1)) 
		{
			if (goTargetted != null) 
			{
				PullObject();
			}
		}
		else if(Input.GetMouseButtonUp(1))
		{
			if(goGrabbed != null)
			{
				ReleaseObject();
			}
		}
	}

	private void PullObject()
	{
		goGrabbed = goTargetted;
		goTargetted = null;
		goGrabbed.layer = 9;
		goGrabbed.transform.parent = transform;
		goGrabbed.GetComponent<Rigidbody>().useGravity = false;
		tweenPull = goGrabbed.GetComponent<Rigidbody>().DOMove(goHands.transform.position, 0.5f).SetEase(Ease.InOutQuint);
	}

	private void ReleaseObject()
	{
		if(tweenPull != null)
		{
			tweenPull.Kill();
		}

		goGrabbed.layer = 8;
		goGrabbed.transform.parent = null;
		goGrabbed.GetComponent<Rigidbody>().useGravity = true;
		Debug.Log("asd");
	}
}
