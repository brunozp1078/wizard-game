﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabGun : MonoBehaviour {

	public string fireButton = "Fire1";
	public float grabDistance = 10.0f;
	public Transform holdPosition;
	public float throwForce  = 100f;
	public ForceMode throwForceMode;
	public LayerMask layerMask = -1;

	private GameObject heldObject = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawRay (transform.position, transform.forward, Color.red);
		if (heldObject == null) {
			
			if (Input.GetButtonDown (fireButton)) 
			{
				Debug.Log ("click");
				RaycastHit hit;

				if (Physics.Raycast (transform.position, transform.forward, out hit, grabDistance, layerMask)) {
					heldObject = hit.collider.gameObject;
					heldObject.GetComponent <Rigidbody> ().isKinematic = true;
					Debug.Log ("agarro");

				}               
			}
		} else
		{
			heldObject.transform.position = holdPosition.position;
		}	
	}
}
