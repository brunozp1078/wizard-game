﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorCarac : MonoBehaviour {

    public Transform player;
    public float throwForce = 10;
    bool hasPlayer = false;
    bool beingCarried = false;
    private bool touched = false;
    public Transform cameraPlayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float dist = Vector3.Distance(gameObject.transform.position, player.position);
        if(dist <= 2.5f)
        {
            hasPlayer = true;
        } else
        {
            hasPlayer = false;
        }
        if (hasPlayer && Input.GetKey(KeyCode.Q))
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = cameraPlayer;
            beingCarried = true;
        }
        if (beingCarried)
        {
            if (touched)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                touched = false;
            }
            if (Input.GetMouseButtonDown(0))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                GetComponent<Rigidbody>().AddForce(cameraPlayer.forward * throwForce);
            }else if (Input.GetMouseButtonDown(1))

            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }

        }

    }

    private void OnTriggerEnter()
    {
        if (beingCarried)
        {
            touched = true;
        }  
    }
}
